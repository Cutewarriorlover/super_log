A simple logger, complete with timers, logging tables, and more!

## Usage

A simple usage example:

```dart
import 'package:super_log/super_log.dart';

main() {
  var logger = new Logger();
  
  logger.info("An information message here.");
}
```

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: https://gitea.com/Cutewarriorlover/super_log/issues
